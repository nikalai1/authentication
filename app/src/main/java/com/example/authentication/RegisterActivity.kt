package com.example.authentication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var editTextEmail: EditText
    private lateinit var editTextPassword: EditText
    private lateinit var editTextPassword2: EditText
    private lateinit var buttonSubmit: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()

        registerListener()
    }

    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextPassword = findViewById(R.id.editTextPassword)
        editTextPassword2 = findViewById(R.id.editTextPassword2)
        buttonSubmit = findViewById(R.id.buttonSubmit)
    }

    private fun registerListener() {
        buttonSubmit.setOnClickListener {
            val email = editTextEmail.text.toString()

            if (email.isEmpty()) {
                Toast.makeText(this, "გთხოვთ შეიყვანოთ E-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (!email.contains("@")) {
                Toast.makeText(this, "E-mail არასწორია", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            val password = editTextPassword.text.toString()
            val password2 = editTextPassword2.text.toString()
            if (password != password2) {
                Toast.makeText(this, "პაროლები არ ემთხვევა ერთმანეთს", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            if (password.isEmpty()) {
                Toast.makeText(this, "გთხოვთ შეიყვანოთ პაროლი", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 9) {
                Toast.makeText(this, "პაროლი უნდა შეიცავდეს მინიმუმ 9 სიმბოლოს", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }

            if (!password.contains(Regex("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%\\-_?&])(?=\\S+$).{9,}"))) {
                Toast.makeText(
                    this,
                    "პაროლი უნდა შეიცავდეს ციფრებსა და სიმბოლოებს !!",
                    Toast.LENGTH_SHORT
                ).show()
                return@setOnClickListener
            }

            Toast.makeText(
                this,
                "პაროლები ემთხვევა ერთმანეთს. წარმატებით დარეგისტრირდით",
                Toast.LENGTH_SHORT
            ).show()

            FirebaseAuth.getInstance()
                .createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val intent = Intent(this, RegisterActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                }
        }
    }
}
